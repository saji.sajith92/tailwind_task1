document.getElementById("menu-toggle").onclick = function () {
  document.getElementById("menu").classList.toggle("show");
};

var a = document.querySelectorAll("#pricing-tab li");
for (var i = 0, length = a.length; i < length; i++) {
  a[i].onclick = function () {
    var b = document.querySelector("#pricing-tab li.active");
    if (b) b.classList.remove("active");
    this.classList.add("active");
  };
}

var tab = document.getElementsByClassName("footer-tab");
var i;

for (i = 0; i < tab.length; i++) {
  tab[i].addEventListener("click", function () {
    this.classList.toggle("active");
    var span = this.childNodes[1];
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
      span.innerHTML = "<i class='fa-solid fa-plus'></i>";
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
      span.innerHTML = "<i class='fa-solid fa-minus'></i>";
    }
  });
}
