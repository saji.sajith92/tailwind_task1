module.exports = {
  content: ["index.html", "js/main.js"],
  theme: {
    extend: {
      colors: {
        themeblue: "#3E9FE3",
        themebluehover: "rgba(0, 117, 200, 0.8)",
        menutext: "#333333",
      },
      borderRadius: {
        lg: "30px",
      },
      fontSize: {
        mdnewsletter: "22px",
        lgnewsletter: "36px",
        bannerpara: "24px",
        footerheading: "22px",
        footermenu: "18px",
        pricingtabale: "18px",
        menufont: "18px",
      },
      fontFamily: {
        body: ['"Roboto"'],
      },
      width: {
        25: "25px",
        33: "33px",
      },
      height: {
        25: "25px",
        33: "33px",
      },
    },
    fontSize: {
      sm: ["14px", "16px"],
      base: ["16px", "24px"],
      lg: ["32px", "41px"],
      xl: ["60px", "56px"],
    },
    screens: {
      sm: "640px",
      md: "768px",
      lg: "1024px",
      xl: "1109px",
    },
    backgroundSize: {
      auto: "auto",
      cover: "cover",
      contain: "contain",
      "100%": "100%",
    },
  },
  plugins: [],
};
